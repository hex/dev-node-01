var manteniment = function (req, res) {
  res.status(403).send('Parada general per manteniment. Perdoni les molesties.');
};

module.exports = {
  manteniment: manteniment,
};
