module.exports = function (io) {
  io.on('connection', function(socket) {
    console.log("socket new: " + socket.id);
    socket.on('disconnect', function(e) {
      console.log("socket end: " + socket.id);
    });
  });
}
