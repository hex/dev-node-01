var mongoose = require('mongoose');

/*
if (process.env.NODE_ENV === 'development') {
	var dbURI = 'mongodb://localhost/test';
} else {
	var dbURI = process.env.MONGOLAB_URI;
}
*/

var dbURI = 'mongodb://dev-01:admin1991@ds031882.mongolab.com:31882/db1';
mongoose.connect(dbURI);

var Item = mongoose.model('Item', {
  name : { type: String, lowercase: true },
  price : Number,
  user : String,
  date : { type: Date, default: Date.now },
  type : {type: String, enum: ['Venta', 'Compra', 'Cambio', 'Servicio']},
  premium : Boolean,
  status : Boolean,
  description : String,
  images: {
    md5 : String
  }
});

var User = mongoose.model('User', {
  name : String,
  password : String,
  surname : String,
  address : String,
  country : String,
  status : Boolean,
  date : { type: Date, default: Date.now },
  comments : String,
  telefon : Number,
  mail : { type: String, lowercase: true },
  contact: String,
  adm_info : String,
  items: String
});

module.exports = {
  item: Item,
  user: User
};
