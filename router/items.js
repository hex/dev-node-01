var express = require('express');
var router = express.Router();
var Model = require('../model/model');
var Item = Model.item;

// Identificacio
/*
router.use(function (req, res, next) {
  console.log("Chequeig d'usuari identificat");
});
*/
// OBTENIR ITEMS
router.get('/', function(req, res) {
  console.log("Items request.");
  Item.find({}, function(req, doc) {
    res.json(doc);
  }).limit(10);
});

// CREAR ITEM
router.post('/', function(req, res) {
    var item = new Item(req.body);
    item.save(function (err) {
      if (err) {
        var retorn = { transfer : false }
        console.log("Item new error: " + handleError(err));
      } else {
        var retorn = { transfer : true }
        console.log("Item new: true")
      }
      res.json(retorn);
    });
});

router.get('/:item_id', function(req, res) {
    console.log("Item request: " + req.params.item_id);
    Item.findById(req.params.item_id, function (err, item) {
      if (err) {
        item = [];
      }
      res.json(item);
    });
});

router.put('/:item_id', function(req, res) {
  Item.findById({ _id : req.params.item_id }, function (err, item) {
    if (err) res.send(err);
    item.name = req.body.name;
    item.save(function (err) {
      if (err) res.send(err);
      res.json({ transfer : true });
    });
  });
});

router.delete('/:item_id', function(req, res) {
  Item.remove({ _id : req.params.item_id }, function (err, item) {
    if (err) res.send(err);
      res.json({ delete : true })
    });
});

module.exports = router;
