var express = require('express');
var router = express.Router();
var Model = require('../model/model');
var User = Model.user;

// Identificacio
/*
router.use(function (req, res, next) {
  console.log("Chequeig d'usuari identificat");
});
*/
// OBTENIR USERS
router.get('/', function(req, res) {
  console.log("Users request.");
  User.find({}, function(req, doc) {
    res.json(doc);
  }).limit(10);
});

// CREAR USER
router.post('/', function(req, res) {
    var user = new User(req.body);
    user.save(function (err) {
      if (err) {
        var retorn = { transfer : false }
        console.log("User new error: " + handleError(err));
      } else {
        var retorn = { transfer : true }
        console.log("User new: true")
      }
      res.json(retorn);
    });
});

// OBTENIR USER
router.get('/:user_id', function(req, res) {
    console.log("User request: " + req.params.user_id);
    User.findById(req.params.user_id, function (err, user) {
      if (err) {
        user = [];
      }
      res.json(user);
    });
});

// EDITAR USER
router.put('/:user_id', function(req, res) {
  User.findById({ _id : req.params.user_id }, function (err, user) {
    if (err) res.send(err);
    user.name = req.body.name;
    user.save(function (err) {
      if (err) res.send(err);
      res.json({ transfer : true });
    });
  });
});


// ELIMINAR USER
router.delete('/:user_id', function(req, res) {
  User.remove({ _id : req.params.user_id }, function (err, user) {
    if (err) res.send(err);
      res.json({ delete : true })
    });
});

module.exports = router;
