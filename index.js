// Sistema
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
//var urlencodedParser = bodyParser.urlencoded({ extended: false });
//var jsonParser = bodyParser.json();

// Codi propi
var main = require('./router/main');
//var items = require('./router/items');
//var users = require('./router/users');
var middleware = require('./middleware/middleware');
require('./sockets/main')(io);

// Configuracio del server
app.set('port', (process.env.PORT || 5000));
app.set('views', './views');
app.set('view engine', 'jade');

//app.use(middleware.manteniment);

// Frontend
//app.use('/public', express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/public'));
app.use('/bower_components',  express.static(__dirname + '/public/bower_components'));

// BODY-PARSER
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

app.use('/', main);

// API
//app.use('/api/items/', items);
//app.use('/api/users/', users);

server.listen(app.get('port'), function () {
    console.log('Servidor corrent al port: ' + app.get('port'));
});
